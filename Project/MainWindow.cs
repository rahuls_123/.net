﻿using System;
using System.IO;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnClearClicked(object sender, EventArgs e)
    {
        //code executed when the Clear button is clicked
        textview6.Buffer.Text = ""; //clears the buffer displayed by the TextView
    }

    protected void OnUppercaseClicked(object sender, EventArgs e)
    {
        //code executed when the Upper Case button is clicked
        textview6.Buffer.Text = textview6.Buffer.Text.ToUpper();
    }

    protected void OnLowercaseClicked(object sender, EventArgs e)
    {
        //code executed when the Lower Case button is clicked
        textview6.Buffer.Text = textview6.Buffer.Text.ToLower();
    }

    protected void OnSaveClicked(object sender, EventArgs e)
    {
        //code executed when the Copy button is clicked
        var sw = new StreamWriter("Test.txt");
        sw.Write(textview6.Buffer.Text); //Write textview1 text to file
        textview6.Buffer.Text = "Saved to file !"; //Notify user
        sw.Close();
    }
}
